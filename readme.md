Subventure
==========
Subventure was created by I, also known as Airglow Studios, to provide an adventure to any seeking it. Subventure is, as of writing, in the stable version of 1.1.1.
A complete list of commands is available in the commands.md file.

Playing Subventure
==================
Subventure is not like most RPGs. There is no fighting, (yet,) no killing, and not much action at all. This may sound boring, and it probably is. Subventure is about adventure, which may be easy to deduce from the title. The player can travel through worlds, made by other players, and the areas in them. Playing is easy.

To download the latest release of Subventure, use these commands in your shell:

```bash
git clone https://github.com/sirsnowy7/subventure.git subventure
cd subventure
chmod a+x subventure.rb
# for fast access, a symlink
ln -s /path/to/subventure.rb /usr/bin/subventure
```

From there, you can run the program with `subventure` or, if you didn't create the symlink, `./subventure.rb`

Or, even simpler, download the raw of `install.sh` and call that. It installs Subventure to ~/.subventure and automatically adds a symlink. It porbably won't work on Windows machines, though.

You will also need a world file. One is available in the worlds/official/ folder, but it's not much more than just an incomplete demo as of now. You can, however, make your own Subventure, and the documentation for that is available in the `worlds.md` file.
