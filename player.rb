class Player
  attr_accessor :name, :spawn, :locationid
  
  def initialize(n, s)
    @name  = n # should be a string
    @spawn = s # should be an area ID
    
    @locationid = @spawn
  end
  
  def location()
    Area.areas.each do |loc|
      if loc.id == self.locationid
        return loc
      end
    end
  end
  
end
