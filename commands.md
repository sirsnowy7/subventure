Commands
========
This is a complete list of Subventure commands with how to use them and their arguments. If you're ever confused on how to use a command, see this document. Also, commands are not case sensitive: move, Move, and MOVE are all the same thing.

Basic
=====
Basic commands are any that require no SU powers, and can be executed with one simple argument.

Move
----
Move is the command that lets you complete the essential tasks of Subventure, like moving. To use this command, type `move` and the location number of where you want to go, as identified in the `Exits lead to these areas:` bit.

Say
---
Say is another very simple command. Type `say` and whatever you want to say, and it echos it to terminal. Very, very simple.

Name
----
Name is how you find your name and also change it. To learn your name, in case you didn't know it already, simply type `name`. To change it, type `name` and the name you want to be called. For instance, `name Chris`. It's useful if you made a typo.

Super
=====
The super user commands are commands you wouldn't normally be able to use in a regular game, for instance, debugging tools, or cheats.

Super
-----
Super is the easiest super user command by far. All it does is identify you as a superuser, so you can use the other SU commands.

Warp
----
Warp is another simple one: to use it, type in warp and the ID of the room you're warping to. One small piece of advice, too. If you like exploring, don't use this command. You may see something you don't want to see. And, by the way, warp is not the same as move.

Special
=======
Special commands are any that pertain to the real world, like saving, quitting, and changing stories.

Quit
----
Quit is the only special command, currently. All it does is close the game, and it is recommended over the keyboard break shortcut, (ctrl+C,) mostly for a graceful closing.
