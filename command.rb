class Command
  @@commands = []
  
  def self.commands
    @@commands
  end

  attr_accessor :name, :code
  
  def initialize(n, c)
    @name = n # should be a string
    @code = c # should be a proc
    @@commands.push(self)
  end
  
  def call(player, command_arg)
    self.code.call(player, command_arg)
  end
  
  def self.main()
    move = Proc.new { |player, arg|
      unless arg.nil?
        if arg.to_i <= $count
          player.locationid = player.location.exits[arg.to_i - 1].id
        else
          puts "That area isn't on the list..."
        end
      else
        puts "That's not a location."
      end }
    
    say = Proc.new { |player, arg|
      unless arg.nil?
        puts player.name + " says, \"" + arg + "\""
      else
        puts "I can't say that!" 
      end }
    
    name = Proc.new { |player, arg|
      unless arg.nil?
        player.name = arg
        puts "Your new name is: " + player.name
      else
        puts "Your name is: " + player.name
      end }
    
    sudo = Proc.new { |player, arg|
      print "Are you sure you can handle the powers of sudo mode? It may be buggy. (y/n): "
      if gets.chomp.downcase == "y"
        puts "You are ready then, young one."
        Subventure.meta["su"] = true
      else
        puts "Okay, traveller. Not this time. Be careful."
      end }
    
    warp = Proc.new { |player, arg|
      if Subventure.meta["su"]
        unless arg.nil?
          unless Area.find_loc_byid(arg.to_i) == 1
            player.locationid = arg.to_i
          else
            puts "Area doesn't exist."
          end
        else
          puts "That's not a location."
        end
      else
        puts "You don't have that power."
      end }
    
    quit = Proc.new { |player, arg|
      print "Are you sure you want to quit? y/n: "
      exit(0) if gets.chomp == "y" }

    Command.new("move", move)
    Command.new("say", say)
    Command.new("name", name)
    Command.new("super", sudo)
    Command.new("warp", warp)
    Command.new("quit", quit)
  end
end
