#!/usr/bin/ruby

require 'io/console'
require_relative 'loader'
require_relative 'player'
require_relative 'area'
require_relative 'command'

def pputs(*s)
  width = IO.console.winsize[1]
  s.each do |str|
    str = str.gsub(/(.{1,#{width}})(\s+|\Z)/, "\\1\n")
    puts str
  end
end

class Subventure
  @@meta  = { "name" => "Subventure", "version" => "1.1.1", "su" => true }
  def self.meta()
    return @@meta
  end
  
  def self.cut_command(command)
    command_typ = ""
    count = 0
    command.each_char do |char|
      if char == " "
        command_arg = command[(count)..-1]
        command_arg = command_arg.reverse.chop.reverse
        return command_typ, command_arg
      end
      command_typ += char
      count += 1
    end
  end

  def self.main(options=[])
    puts ""
    Loader.main
    Command.main
    puts ""
    print "Please enter your name: "
    player = Player.new(gets.chomp, 0)
    print "Welcome, ", player.name, ", to an adventure.\n\n"
    sleep 0.5
    loop do
      pputs "", player.location.title, player.location.desc
      $count = 0
      puts "", "Exits lead to these areas: "
      player.location.exits.each do |room|
        $count += 1
        print $count, ". ", room.title, "\n"
      end
      puts "\n"
      print Subventure.name, " >> "
      command = gets.chomp
      puts ""
      if command.length >= 1
        command_typ, command_arg = Subventure.cut_command(command)
        begin
          Command.commands.each do |com|
            if command_typ == com.name
              com.call(player, command_arg)
              $found_com = true
              break
            end
          end
          unless $found_com
            puts "Command not recognized..."
          end
          $found_com = nil
        rescue
          puts "Command execution failed: you shouldn't be seeing this message, by the way. Please open a GitHub issue with a description of what you were trying to do."
        end
      end
    end
  end
end

begin
  Subventure.main
rescue Interrupt
  puts "\nQuitting... ( and by the way, you should use the quit command to quit. )"
  exit(1)
end
