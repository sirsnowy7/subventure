class Area
  @@areas  = []
  def self.areas
    @@areas
  end
  
  attr_accessor :id, :title, :desc, :around, :labels, :items, :npcs
  
  def initialize(id, t, d, a, l=[], i=[], n=[])
    @id     = id
    @title  = t # should be a string
    @desc   = d # should be a string
    @around = a # should be an array (of IDs)
    @labels = l # should be an array of strings
    @items  = i # should be an array (of IDs)
    @npcs   = n # should be an array (of IDs)
    @@areas.push(self)
  end
  
  def exits
    output = []
    loc_around = self.around
    Area.areas.each do |loc|
      if loc_around.include?(loc.id)
        found_area = Area.find_loc(loc)
        output.push(found_area) if found_area != 0
      end
    end
    return output
  end
  
  def self.find_loc(location)
    Area.areas.each do |loc|
      if loc.id == location.id
        return loc
      end
    end
    return 1
  end
  
  def self.find_loc_byid(id)
    Area.areas.each do |loc|
      if loc.id == id
        return true
      end
    end
    return 1
  end
  
end
