require 'json'

class Loader
  def self.main()
    print "World file to load: "
    file = gets.chomp
    file = File.open(file, "r")
    content = file.read.force_encoding("ISO-8859-1")
    world = JSON.parse(content)
    areas = []
    world.each do |x|
      x = x[1]
      unreq_a = [ x["labels"], x["items"], x["npcs"] ]
      unreq_a.each do |s|
        if s.nil?
          s = []
        end
      end 
      Area.new(
        x["id"],
        x["title"],
        x["desc"],
        x["around"],
        x["labels"],
        x["items"],
        x["npcs"] )
    end
  end
end
