Subventure Worlds
=================
Worlds are created using the Javascript Object Notation Syntax, commonly known as JSON. They are very easy to create, and quite simple, too. First, you're going to want to learn JSON, so you're familiar with the syntax Subventure uses for world creation. A sample is available in the worlds folder, as well as a template.

All world files should begin as just this:

```JSON
{
}
```

Then, you'll add an area you want to create. Each area uses an ID, a title, a description, and an "around." It does not have to include the unrequired ones, which are "items" and "npcs." When you fill these in, you'll have this (if you included the unrequired fields):

```JSON
{

	"room_name":{"id":0, "title":"Spawn Room",
	"desc":"Room description should be longer than this.",
	"around":[10, 20, 30], "items":[], "npcs":[] }

}
```

Notice that "items" and "npcs" don't have anything in them, but they are just empty arrays. This is fine, because they aren't needed, in fact, they don't do anything at all yet.

Also, the spawn room is always zero, and this should be kept in mind. You have to have an area with "id":0 in your world file, or it won't work.

ID
--
The ID is simply the ID of the room. It is referred to by the `"around"` field. It is best to use numbers in this field, but strings and other objects should work too. That said, I don't recommend that, because that is not reliable and may break in the future. You have been warned. All IDs must be unique between every room.

Also, 0 is always the spawn point, as of now, so your worlds must include an area with an ID of 0.

It is also probably best to use increments of 10, but that is not a set-in-stone rule, so feel free to use whatever you want as an increment. 

Title
-----
The title is just the name of the room. Something like, "The Great Hall," or "Wicked Garden." You get the point. Just don't make these titles too long, and everything should be okay.

Description
-----------
The description should be longer than the title, and probably longer than one line. Since this is a text-adventure game, everything counts on the description. Really give it your all. Other than that, there are np rules, just be descriptive. After all, it's a description, right?

Around
------
Around is a bit more confusing to understand. In fact, you might want to map your areas out. Basically, it works like this:

```
+-+-+-+
|1 2|3|
+ +-+ +
|4 5 6|
+-+-+ +
|7 8 9|
+-+-+-+
```

As you can see, 5 has two areas around it, 5 and 6. This could be written as, `"around":[4, 6]`. 

Every area has areas around it:
1. 2, 4
2. 1
3. 6
4. 1, 5
5. 4, 6
6. 3, 9
7. 8
8. 7, 9
9. 8, 6

There are **no** limits to the amount of areas in your around, but keep in mind it's easier to manage a smaller number.

Labels
------
Labels are, quite simply, the labels which are linked to every room in your "around" property. The labels are what is displayed for the area when listed as an exit, which defaults to the title of said "around" area. These are currently inactive and not used, nor required.

Items & NPCs
------------
Items and NPCs have no importance yet, because they are not fully implemented. You cannot use them, so it is better to leave the fields as just `"item":[]` and `"npcs":[]`, so there are no compatability issues in the future. These fields, though, aren't required to even be there, as of version 1.1.

World Tips & Techniques
-----------------------
Worlds should be open. Since this is Subventure, there isn't much else to do than adventure the world you live in. So, every area should lead somewhere, or tell you something about the world, and there should be many different areas to adventure.

Secondly, use sensory details frequently. Make the words you use fit the world you create. Remember that the essence of Subventure really is the sensory detail. For instance, in a open, sunny plain, you can feel the suns delicate light touch you, maybe a steady breeze, cooling your skin, the smell of grass fresh in the lands; but that is only one part of what you can do with your words.

You may have been thinking, is this just for sensory, open adventures? No, it is not. For example, a choose-your-own-adventure game would be easy in Subventure. Making a maze would not be difficult either, if not trivial (as soon as labels are functional.)

Finally, the warp command could take you to a secret location, if you know the ID (which you should, if you made the world.) This can be great for debug. Simply make a room with the around pointing to all of your rooms or just the major crossroads. You can then just warp to that room by entering superuser mode, (command is `super`,) and then entering the command to warp, which would be `warp` and whatever the secret ID is. Like 0, for the Null room in sample.json, that the player can't access after leaving the spawn point.

Submitting Worlds
-----------------
Submitting a world to the community is easy! Simply open a pull request with your world in the `worlds/player_worlds` folder.

Guidelines for submitting:
- Make sure your world is, at least, 70% done, 50% done for very large worlds.
- Each world must have a spawn area.
- The JSON must parse without error.

I, Snowy, will play every adventure that is submitted, personally. I will, and not just to enforce the rules, but because I would love seeing what people have created for, well, my creation, Subventure. If you're wondering if something is okay to submit, just make a PR. I'll review it. Or, if you'd like for it to be more private, you can send the file to my [email](http://savagesans@gmail.com).

And, note that no whitespace is needed, but if you want to submit it, I will either add whitespace before I merge the pull request, or you will have to. It makes the JSON much more easier to read and edit.
